FROM nvidia/cuda:11.8.0-devel-ubuntu22.04 as base

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV DEBIAN_FRONTEND=noninteractive \
    TZ=Europe/Madrid \
    PYTHONUNBUFFERED=1 \
    PIP_NO_CACHE_DIR=on \
    SHELL=/bin/bash


# PYTHON INSTALL
FROM base as python
RUN apt update -y && apt upgrade -y
RUN apt install -y software-properties-common vim git wget curl && \
	add-apt-repository ppa:deadsnakes/ppa -y && \
	apt update -y
RUN apt install -y libgl1 python3.10 python3.10-dev python3.10-venv python3-pip python3-tk && \
	apt clean && \
	rm -rf /var/lib/apt/lists/* && \
	echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN ln -fs /usr/bin/python3 /bin/python


# RUNPODCTL
FROM python as runpodctl
RUN wget https://github.com/Run-Pod/runpodctl/releases/download/v1.10.0/runpodctl-linux-amd -O runpodctl && chmod +x runpodctl && cp runpodctl /usr/bin/runpodctl


# CLIP INTERROGATOR
FROM runpodctl as clip_interrogator
RUN adduser --shell /bin/bash --gecos "" --disabled-password clip_interrogator
USER clip_interrogator
ENV PATH="/home/clip_interrogator/.local/bin:$PATH"
WORKDIR /home/clip_interrogator
COPY --chown=clip_interrogator:clip_interrogator scripts/Clip_Interrogator.py .
RUN python -m venv --system-site-packages venv && \
	. venv/bin/activate && \
	pip install --user open_clip_torch torch==2.0.1 torchvision torchaudio -f https://download.pytorch.org/whl/cu118 && \
	pip install --user transformers bitsandbytes accelerate clip-interrogator==0.6.0 gradio && \
	deactivate


# JUPYTER
FROM clip_interrogator as jupyterlab
RUN pip install --user -U --no-cache-dir jupyterlab \
        jupyterlab_widgets \
        ipykernel \
        ipywidgets \
        gdown
USER root
RUN mkdir /workspace
RUN chown -R clip_interrogator. /workspace && chmod -R 755 /workspace


# SETUP SCRIPTS
COPY --chown=clip_interrogator:clip_interrogator scripts/entrypoint.sh /bin/entrypoint
COPY scripts/az.sh /bin/ci

WORKDIR /workspace

USER clip_interrogator

CMD ["/bin/bash"]
ENTRYPOINT ["/bin/entrypoint"]
