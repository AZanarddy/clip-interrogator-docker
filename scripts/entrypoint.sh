#!/usr/bin/env bash

set -e

export PYTHONUNBUFFERED=1

start_jupyter() {
	echo "Starting Jupyter Lab..."
	mkdir -p /workspace/{logs,images} && \
        cd / && \
        nohup jupyter lab \
          --no-browser \
          --port=8888 \
          --ip=* \
          --FileContentsManager.delete_to_trash=False \
          --ContentsManager.allow_hidden=True \
          --ServerApp.terminado_settings='{"shell_command":["/bin/bash"]}' \
          --ServerApp.token=${JUPYTER_PASSWORD:-P1t@gor4s} \
          --ServerApp.allow_origin=* \
          --ServerApp.preferred_dir=/workspace &> /workspace/logs/jupyter.log &
        echo "Jupyter Lab started"
}

start_jupyter

while true; do
	sleep 1;
	/bin/bash
done
