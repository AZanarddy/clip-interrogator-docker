#!/usr/bin/env bash

set -e

export PYTHONUNBUFFERED=1


start_clip_interrogator() {
	cd; . venv/bin/activate

	python Clip_Interrogator.py --share
}


if [[ ${#} -lt 1 || ${#} -gt 2 ]]; then
    echo 'Usage: ci start'
    exit 1
fi

case ${1} in
	start)
		start_clip_interrogator
		;;
    *)
        echo 'Valid options: start'
        ;;
esac

